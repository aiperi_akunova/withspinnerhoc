import React from 'react';
import './Posts.css';

const Posts = props => {
    return (
        <div className='post'>
          <p><b>Created on:  </b>{props.time}</p>
            <h3>{props.title}</h3>
            <p>{props.text}</p>
        </div>
    );
};

export default Posts;