import React, {useEffect, useState} from 'react';
import axios from "axios";
import Posts from "../../components/Posts/Posts";
import withSpinner from "../../hoc/withSpinner/withSpinner"

const Home = () => {
    const [posts, setPosts] = useState([]);

    useEffect(()=>{
        const fetchData = async ()=>{
            const response = await axios.get('https://blog-93444-default-rtdb.firebaseio.com/posts.json');
            const incomingPosts = response.data;
            setPosts(incomingPosts);
            console.log(response)
        }
        fetchData().catch(e => console.error(e));

    },[]);
    return (
        <div className='home'>
            <h2>My posts</h2>
            {Object.keys(posts).map((item, i) => (
                <Posts
                    title = {posts[item].title}
                    text = {posts[item].text}
                    time={posts[item].time}
                    key={item}
                    id={item}
                />
            ))
            }
        </div>
    );
};

export default withSpinner(Home, axios);