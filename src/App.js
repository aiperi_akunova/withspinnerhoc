import './App.css';
import Home from "./container/ Home/Home";

const App = () => {

    return (
        <div className="App">
            <Home/>
        </div>
    );
};

export default App;
