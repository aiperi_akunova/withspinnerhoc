import React, { useMemo, useState} from 'react';
import Spinner from "../../components/Spinner/Spinner";

const WithSpinner = (WrappedComponent, axios) => {

    return WithSpinnerHOC;

    function WithSpinnerHOC(props) {
        const [loading, setLoading] = useState(false);

        let spinner = null;

        useMemo(() => {

            axios.interceptors.request.use(req => {
                setLoading(true);
                return req;
            })

            axios.interceptors.response.use(res => {
                setLoading(false);
                return res;
            }, err => {
                setLoading(false);
                throw err;
            })

        }, []);

        if (loading) {
            spinner = <Spinner/>
        }
        return (
            <>
                <WrappedComponent {...props} />
                {spinner}
            </>
        )

    }
};

export default WithSpinner;